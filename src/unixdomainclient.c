#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "unixdomainclient.h"
#include "log.h"

#define SERVER_PATH "tpf_unix_sock.server"
#define DATA "Hello from client"




int start_unix_domain_stream_client(){

    int client_sock, rc, len;
    struct sockaddr_un server_sockaddr; 
    struct sockaddr_un client_sockaddr; 
    char buf[256];
    memset(&server_sockaddr, 0, sizeof(struct sockaddr_un));
    memset(&client_sockaddr, 0, sizeof(struct sockaddr_un));
     
    
    client_sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (client_sock == -1) {
        log_error("SOCKET ERROR");
    }

    char filePath[256];
    sprintf(filePath, "%s%d", CLIENT_PATH, getpid());
    
    client_sockaddr.sun_family = AF_UNIX;   
    strcpy(client_sockaddr.sun_path, filePath); 
    len = sizeof(client_sockaddr);
    
    unlink(CLIENT_PATH);
    rc = bind(client_sock, (struct sockaddr *) &client_sockaddr, len);
    if (rc == -1){
        log_error("BIND ERROR");
        close(client_sock);
        //exit(1);
    }
        

    
    server_sockaddr.sun_family = AF_UNIX;
    strcpy(server_sockaddr.sun_path, SERVER_PATH);
    rc = connect(client_sock, (struct sockaddr *) &server_sockaddr, len);
    if(rc == -1){
        log_error("CONNECT ERROR");
        close(client_sock);
        //exit(1);
    }
           
    return client_sock;
}
