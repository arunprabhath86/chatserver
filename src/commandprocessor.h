#ifndef _COMMAND_PROCESSOR_H
#define _COMMAND_PROCESSOR_H

#include "linkedlist.h"

#define CMD_JOIN "JOIN"
#define CMD_CHAT "CHAT"
#define CMD_LEAVE "LEAVE"
#define CMD_EXIT "EXIT"

#define INVALID_CMD -1
#define CLOSE_CONNECTION 0
#define PROCESSED 1
#define JOINED 2
#define ALREADY_JOINED_ERROR -2
#define LEAVE_ERROR -3
#define EXIT_ERROR -4


struct serv_commd_arg {
    int *unixDomainFd;
    List *connectionStore;
};


int process_command(char *, List *, int , int *, char *);
void process_server_command(void * arg);


#endif