#ifndef _RESOURCELOCK_H_
#define _RESOURCELOCK_H_

void resource_lock_init();
void resource_lock_wait();
void resource_lock_release();
#endif