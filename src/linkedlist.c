#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "linkedlist.h"
#include "log.h"

struct node {
	int            *connfd;
	char           *nick_name;
	struct node    *next;
};

struct list {
	Node           *head;
};

pthread_mutex_t lock;

Node  *createnode(int *newConnfd, char *nickname);

Node *createnode(int *newConnfd, char *nickname) {
	Node           *newNode = (Node *)malloc(sizeof(Node));
	if (!newNode) {
		return NULL;
	}
	newNode->connfd = newConnfd;
	newNode->nick_name = nickname;
	newNode->next = NULL;
	return newNode;
}

List  *createNewList() {
  pthread_mutex_init(&lock, NULL);
	List           *list = (List *)malloc(sizeof(List));
	if (!list) {
		return NULL;
	}
	list->head = NULL;
	return list;
}

void display_list(List * list) {
	Node *current = list->head;
	if (list->head == NULL)
		return;

	for (; current != NULL; current = current->next) {
		log_info("[%s : %u]", current->nick_name, current->connfd);
	}
}

void add_to_list(char *nickname, int *connfd, List * list) {
  pthread_mutex_lock(&lock);
	Node *current = NULL;
	if (list->head == NULL) {
		list->head = createnode(connfd, nickname);
	} else {
		current = list->head;
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = createnode(connfd, nickname);
	}
  pthread_mutex_unlock(&lock);
}

void delete_from_list(char *nickname, List * list) {
	Node  *current = list->head;
	Node  *previous = current;
	while (current != NULL) {
		if (strcmp(current->nick_name, nickname) == 0) {
			previous->next = current->next;
			if (current == list->head)
				list->head = current->next;
      pthread_mutex_lock(&lock);
			free(current);
      pthread_mutex_unlock(&lock);
			return;
		}
		previous = current;
		current = current->next;
	}
}


int  *get_connfd_from_list(char *nickname, List * list) {
  int *returnval;
  returnval = NULL;
  pthread_mutex_lock(&lock);
	Node *current = list->head;
	while (current != NULL) {
		if (strcmp(current->nick_name, nickname) == 0) {
      log_info("Returning confd: %d", *(current->connfd));
			returnval =  current->connfd;
      break;
		}
    current = current->next;
	}
  pthread_mutex_unlock(&lock);
	return returnval;
}



void destroy_list(List * list) {
	Node *current = list->head;
	Node *next = current;
	while (current != NULL) {
		next = current->next;
		free(current);
		current = next;
	}
	free(list);
  pthread_mutex_lock(&lock);
  pthread_mutex_destroy(&lock);
}
