#ifndef _UTIL_RESOURCE_H_
#define _UTIL_RESOURCE_H_

#ifndef HAVE_GETRUSAGE_PROTO
int     getrusage(int, struct rusage *);
#endif

void print_cpu_time(void);

#endif