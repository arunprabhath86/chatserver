
#include<semaphore.h>
#include "resourcelock.h"

static sem_t accept_lock;



void resource_lock_init() {
    sem_init(&accept_lock, 1, 1);
}



void resource_lock_wait() {
    sem_wait(&accept_lock);
}

void resource_lock_release(){
    sem_post(&accept_lock);  
}

void free_resource_lock() {
    sem_destroy(&accept_lock);
}
