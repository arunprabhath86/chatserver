#include <string.h>

#include "commandprocessor.h"
#include "linkedlist.h"
#include "log.h"

#define MAX_CMD_WORD_LEGTH 20

int  process_join_command(char *data, List * connection_store, int *inetDomainClientSockfd, char *currentNickName);
int process_leave_command(List * connection_store, char *currentNickName);
int  process_chat_command(char *data, List * connection_store, int unixServerSockFd, char *currentNickName);
void process_server_command(void * arg);
void send_data_to_client(char *data, List *connection_store);
size_t split(char *buffer, char *argv[], size_t argv_size);
char *substring(char *string, int position, int length);
void chomp(char *s);


/**
* This function process command from each TCP connection
* @param char *data
* @param List * connection_store
* @param int unixServerSockFd
* @param int *inetDomainClientSockfd
* @param char *currentNickName - It will be empty before JOIN command
* @return status - for statuses please refer commandprocessor.h
*/
int process_command(char *data, List * connection_store, int unixServerSockFd, int *inetDomainClientSockfd, char *currentNickName) {
    chomp(data);   
    log_info("Command Recieved: %s ", data);
    if (strncmp(CMD_LEAVE, data, 5) == 0) {
        return process_leave_command(connection_store, currentNickName);
    } else if (strncmp(CMD_JOIN, data, 4) == 0) {
        return process_join_command(data, connection_store, inetDomainClientSockfd, currentNickName);
    } else if (strncmp(CMD_CHAT, data, 4) == 0) {
        return process_chat_command(data, connection_store, unixServerSockFd, currentNickName);
        
    } else if(strncmp(CMD_EXIT, data, 4) == 0) {
        if (currentNickName[0] == '\0' ) {
            return CLOSE_CONNECTION;
        } 
        return EXIT_ERROR;
    } 
    return INVALID_CMD;
}

/*
* This function prcesses command recevied from the parent
* process. Since the target users are in different processes,
* the master process handle the CHAT messages, by brodcasting 
* the CHAT command to all the connected client. On receiving 
* this command in the child process, it will check if the 
* TCP connection is present in the connection store for the 
* particular nickname. If available, this function passes 
* the message to that particular connection. This is a
* thread handler function.
* 
* @param void *arg
*/
void process_server_command(void * arg) {

    struct serv_commd_arg *args = (struct serv_commd_arg *)arg;
    char buf[1024]; 
    int bytes_rec;
    int *unixServerSockFd = (int *) args->unixDomainFd;
    List *connection_store = (List *) args->connectionStore;
    while(1) {
        memset(buf, 0, 1024); 
        bytes_rec = recv(*unixServerSockFd, buf, sizeof(buf), 0);
        if (bytes_rec <= 0){
            close(*unixServerSockFd);
            break;
        }
        chomp(buf);
        if (buf != NULL && strlen(buf) > 3) {
            log_info("Recieved command from the parent process: %s", buf);
            display_list(connection_store);
            send_data_to_client(buf, connection_store);
        }
    }
}

/**
* This function will send the messages to the TCP connection,
* if TCP connection available in the connection store for the 
* particular nickname. If it is not available, the message 
* is discarded.
*
* @param char *data
* @param List *connection_store
*/
void send_data_to_client(char *data, List *connection_store) {

        size_t i, wordCount;
        int len = strlen(data);
        char **cmd_words = malloc(MAX_CMD_WORD_LEGTH * sizeof(char*));
        log_debug("%s and length: %d", data, len);
        wordCount = split(data, cmd_words, MAX_CMD_WORD_LEGTH);
        log_debug("From server word count %d", wordCount);
        if (wordCount > 4) {
            return;
        }
        log_debug("ACTION(%s) NAME(%s) MSG(%s) SENDER(%s)", cmd_words[0], cmd_words[1], cmd_words[2], cmd_words[3]);
        char *nickname = (char *)malloc(strlen(cmd_words[1])+1);
        log_debug("nickname : %s", cmd_words[1]);
        display_list(connection_store);
        int *inetConnFd;
        inetConnFd = get_connfd_from_list(cmd_words[1], connection_store);
        if (inetConnFd != NULL) {
            log_debug("After lookup in datastore tcp fd %d", *inetConnFd);            
            char actalMsg[len * 2];
            memset(actalMsg, '\0', sizeof(actalMsg));
            sprintf(actalMsg, "%s << %s\r\nYou # \0", cmd_words[3], cmd_words[2]); 
            log_debug("Final message: %s Len:%d", actalMsg, sizeof(actalMsg));
            int sendBytes = send(*inetConnFd, actalMsg, sizeof(actalMsg), 0);
            log_info("%d bytes send to TCP client %d", sendBytes, *inetConnFd);
        } else {
            log_info("Connfd is null, do nothing");
        }   
}



char *substring(char *string, int position, int length) {
   char *pointer;
   int c;
 
   pointer = malloc(length+1); 
   if (pointer == NULL) {
      log_error("Unable to allocate memory.\n");      
   }
 
   for (c = 0 ; c < position -1 ; c++) 
      string++; 
 
   for (c = 0 ; c < length ; c++) {
      *(pointer+c) = *string;      
      string++;   
   } 
   *(pointer+c) = '\0'; 
   return pointer;
}

/**
* This funtion removes the CRLF from the string 
*/
void chomp(char *s) {
    while(*s && *s != '\n' && *s != '\r') s++;
 
    *s = 0;
}

/*
* This function handles the CHAT command. On receiving 
* this command from the TCP client, it sends the command
* to the master process via unix domain socket. The master
* process' duty is to broadcast this chat message to all
* spawned processes via unix domain socket connection.
*
* @param char *data
* @param List * connection_store
* @param int unixServerSockFd
* @param char *currentNickName
* @return command status - Please refer commandprocessor.h
*/
int  process_chat_command(char *data, List * connection_store, int unixServerSockFd, char *currentNickName) {
    log_info("currentNickName: %s", currentNickName);
    int com_len = strlen(data);
    if (currentNickName[0] != '\0' ) {
        char broadCastmsg[strlen(data) * 2];
        size_t i, wordCount;
        char **cmd_words = malloc(MAX_CMD_WORD_LEGTH * sizeof(char*));
        wordCount = split(data, cmd_words, MAX_CMD_WORD_LEGTH);
        log_info("wordCount : %d", wordCount);
        if (wordCount != 3) {
            return INVALID_CMD;
        }
        sprintf(broadCastmsg, "%s %s \"%s\" %s", cmd_words[0], cmd_words[1], cmd_words[2], currentNickName); 
        log_debug("Message to unixdomain server in the master process: %s", broadCastmsg);
        log_debug("ACTION(%s) SNAME(%s) DNAME(%s) MSG(%s)", cmd_words[0], currentNickName, cmd_words[1], cmd_words[2]);
        int rc = send(unixServerSockFd, broadCastmsg, strlen(broadCastmsg), 0);
        log_info("%d bytes sent to the unix domain server");        
        return PROCESSED;
    } else {
        return INVALID_CMD;
    }
    



}

/*
* This function handles the LEAVE command. On receiving 
* this command from the TCP client, it will delete the
* connection entries from the connection store and on success
* it sends the close connection status to the caller.* 
*
* @param List * connection_store
* @param char *currentNickName
* @return command status - Please refer commandprocessor.h
*/
int process_leave_command(List * connection_store, char *currentNickName) {
    log_info("currentNickName: %s", currentNickName);
    if (currentNickName[0] != '\0' ) {
        delete_from_list(currentNickName, connection_store);
        display_list(connection_store);
        return CLOSE_CONNECTION;
    } 
    return LEAVE_ERROR;

}

/*
* This function handles the JOIN command. On receiving 
* this command from the TCP client, it will add the
* connection entry to the connection store  
*
* @param char *data
* @param List * connection_store
* @param int *inetDomainClientSockfd
* @param char *currentNickName
* @return command status - Please refer commandprocessor.h
*/
int  process_join_command(char *data, List * connection_store, int *inetDomainClientSockfd, char *currentNickName) {
    log_info("currentNickName: %s", currentNickName);
    if (currentNickName[0] == '\0' ) {
        size_t i, wordCount;
        char **cmd_words = malloc(MAX_CMD_WORD_LEGTH * sizeof(char*));
        wordCount = split(data, cmd_words, MAX_CMD_WORD_LEGTH);
        if (wordCount != 2) {
            return INVALID_CMD;
        }
        char *nickname = (char *)malloc(strlen(cmd_words[1])+1);
        strcpy(nickname, cmd_words[1]);
        nickname[strlen(cmd_words[1])] = '\0';
        strcpy(currentNickName, nickname);
        add_to_list(nickname, inetDomainClientSockfd, connection_store);
        display_list(connection_store);
        return JOINED;
    } else {
        return ALREADY_JOINED_ERROR;
    }
}




/*
* This function extract the words from the string.
* If the word contains the words with quoted, it will be
* treated as a single word.
*/
size_t split(char *buffer, char *argv[], size_t argv_size) {
    char *p, *start_of_word;
    int c;
    enum states { DULL, IN_WORD, IN_STRING } state = DULL;
    size_t argc = 0;

    for (p = buffer; argc < argv_size && *p != '\0'; p++) {
        c = (unsigned char) *p;
        switch (state) {
        case DULL:
            if (isspace(c)) {
                continue;
            }

            if (c == '"') {
                state = IN_STRING;
                start_of_word = p + 1; 
                continue;
            }
            state = IN_WORD;
            start_of_word = p;
            continue;

        case IN_STRING:
            if (c == '"') {
                *p = 0;
                argv[argc++] = start_of_word;
                state = DULL;
            }
            continue;

        case IN_WORD:
            if (isspace(c)) {
                *p = 0;
                argv[argc++] = start_of_word;
                state = DULL;
            }
            continue;
        }
    }

    if (state != DULL && argc < argv_size)
        argv[argc++] = start_of_word;

    return argc;
}

