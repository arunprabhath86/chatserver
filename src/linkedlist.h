#ifndef LINKEDLIST_HEADER
#define LINKEDLIST_HEADER

typedef struct node Node;

typedef struct list List;

List *createNewList();
void add_to_list(char *nickname, int *connfd, List * list);
int  *get_connfd_from_list(char *nickname, List * list);
void delete_from_list(char *nickname, List * list);
void display_list(List * list);
void destroy_list(List * list);

#endif
