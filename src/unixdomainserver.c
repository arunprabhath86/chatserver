#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "threadpool.h"
#include "log.h"

#define SOCK_PATH  "tpf_unix_sock.server"


int start_unix_domain_server(void) {

    int server_sock, len, rc;
    int bytes_rec = 0;
    struct sockaddr_un server_sockaddr;
        
    int backlog = 10;
    memset(&server_sockaddr, 0, sizeof(struct sockaddr_un));
    
    server_sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (server_sock == -1){
        log_error("SOCKET ERROR");
        exit(1);
    }
    
    
    server_sockaddr.sun_family = AF_UNIX;   
    strcpy(server_sockaddr.sun_path, SOCK_PATH); 
    len = sizeof(server_sockaddr);
    
    unlink(SOCK_PATH);
    rc = bind(server_sock, (struct sockaddr *) &server_sockaddr, len);
    if (rc == -1){
        log_error("BIND ERROR" );
        close(server_sock);
        exit(1);
    }
    
    
    /* Listen for any client sockets */    
    rc = listen(server_sock, backlog);
    if (rc == -1){ 
        log_error("LISTEN ERROR");
        close(server_sock);
        exit(1);
    }
    log_info("Unix domain socket is listening");
       
    
    return server_sock;
}



    



