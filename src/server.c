
#include <strings.h>
#include <stdio.h>    
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <signal.h>
#include <errno.h>
#include <sys/resource.h>
#include <sys/un.h>

#include "threadpool.h"
#include "utilresource.h"
#include "resourcelock.h"
#include "linkedlist.h"
#include "log.h"
#include "unixdomainserver.h"
#include "unixdomainclient.h"
#include "commandprocessor.h"

/**
*
* This is the main server function which creates the listening sockets
* and it will be shared accross all the forked clients. The forked 
* client will be accepting connections from the client. The number of
* processes should be created, will be depended on the argument we are 
* using while runinng this server.  
*
* @author Arun Prabhath
* @Date 31 March 2019
*/


#define MAX_BACKLOG 0

#define MAXLINE 1024

#define CONNECT_MESSAGE "You are connected. Please type JOIN <nickname> to the Chat System\r\nYou # "

#define INVALID_COMMAND_MESSAGE "System could not understad the command, Please do: \n (1) CHAT <to_nickname> <\"message\"> \n (2) LEAVE \n (3) JOIN <nickname>\r\nYou # "

#define ALREADY_JOINED_ERR_MSG "You can join only once\r\nYou # "

#define NOT_YET_JOINED "You have not yet joined, type \"EXIT\" to close the connection\r\nYou # "

#define EXIT_ERROR_MSG "Since you have already joined, you have to type \"LEAVE\" in order to exit the session\r\nYou # "

#define JOINED_MSG "You have joined the chat room\r\nYou # "

#define PROMPT "You # "

#define DATA "Hello from server"

/* This is the number of worker processes*/
static int n_worker_process;

/* This stores the process ids of the workker processes*/
static pid_t *worker_pids;

static int tcp_port;

static int num_thread_per_process;
/* Handling the signalsfor both child and parent process */

/*Server unix domain stream socket fd*/
int unix_domain_server_fd;

/*Client unix domain stream socket fd, each 
* spawned process uses it
*/
int unix_domain_client_fd;

/*Interrupt signal handler*/
void  sig_int_handler(int);

/*Function to fork workers*/
pid_t   fork_worker_process(int, int, int);

/*Worker main function*/
void worker_process_main(int, int, int);

/*Function to process client request */
void process_client(void *);

/*Function to accept connection from unix domain socket client*/
void accept_connection_from_unix_domain(void *);

/*Function to handle the request from unix domain socket client*/
void process_unix_domain_client(void *);

/*Threadpool referece which is being used to handle the accept 
* connection from TCP client from spawned processes
**/
threadpool_t *pool;

/*This is a linked list to store the nickname and tcp connection
* FDs in spawned processes
*/
List * connection_store;

/*Array of pointers to hold the connectionFds of unix domain sockets*/
int **unix_domain_connection_fds;

/*It is a counter for unix domain socket connections.*/
int unix_domain_connection_fd_counter = 0;
	

/*Function to listen tcp*/
int listen_tcp(socklen_t *);

/*This reference is for processing unix domain socket connection from 
* each spawned process
*/
pthread_t unix_domain_client_process_thread;
/*It is used to handle the unix domain socket messages from the master
* process. This is being used by all the spawned processes.
*/
pthread_t unix_domain_server_command_process_thread;

/* This is used to handle connection in master process.
*/
threadpool_t *unix_domain_connection_pool;


/* This struct is used as the argument to the tcp connection handler used by
 * each  tcp connection in the spawned processes.
*/
struct arg_struct {
    int *inet_connfd;
    int unix_connfd;
};

/**
 * Start function of the ChatServer
 * @param int argc command line argument length.
 * @param char[] array of commandline arguments
 */
void main(int argc, char ** argv) { 

	int     listenfd, i;
 	socklen_t addrlen;

	if (argc == 4) {
		tcp_port = atoi(argv[1]);
		n_worker_process = atoi(argv[2]);
		num_thread_per_process = atoi(argv[3]);
		listenfd = listen_tcp(&addrlen); 
	} else {
		printf("Usage: ChatServer <#port> <#Worker processes> <#threads>\n");
		exit(0);
	}
	unix_domain_server_fd = start_unix_domain_server();

	pthread_create(&unix_domain_client_process_thread, NULL, &accept_connection_from_unix_domain, NULL); 
	unix_domain_connection_fds = malloc(n_worker_process * sizeof(int *));
	worker_pids = calloc(n_worker_process, sizeof(pid_t));
	resource_lock_init();
	for (i = 0; i < n_worker_process; i++) {
		worker_pids[i] = fork_worker_process(i, listenfd, addrlen); /* parent returns */
	}

	signal(SIGINT, sig_int_handler);

	for ( ; ; ) {	
		pause(); 
	}               
}


/**
 * This function start accepting connection from 
 * the unix domain client. Each spawned process, connects to the 
 * unix domain socket which is listening in parent process.
 *
 * @param void *
*/
void accept_connection_from_unix_domain(void *arg) {
	struct sockaddr_un client_sockaddr; 
	int len = sizeof(client_sockaddr);
	memset(&client_sockaddr, 0, sizeof(struct sockaddr_un));
	if ((unix_domain_connection_pool = threadpool_create(n_worker_process, n_worker_process, 0)) == NULL) {
        log_error("Error creating threadpool");
        exit(0);
    }
    log_info("Threadpool created for accepting unix domain socket connection"); 
	while (1) {
        int *client_sock = (int *)malloc(sizeof(int));
        *client_sock = accept(unix_domain_server_fd, (struct sockaddr *) &client_sockaddr, &len);
        if (client_sock == -1){
            log_error("ACCEPT ERROR");
            //close(server_sock);
            close(client_sock);
            exit(1);
        }
        //TODO add connectionfd to store
        unix_domain_connection_fds[unix_domain_connection_fd_counter] = client_sock;
        unix_domain_connection_fd_counter++;
        threadpool_add(unix_domain_connection_pool, &process_unix_domain_client, client_sock, 0);
    }
}

/**
* This function broadcast messages to all connected unix domain
* socket clients.
* @param char *data
*/
void send_chat_message_to_all_unix_domain_client(char *data) {
	for (int i=0; i< unix_domain_connection_fd_counter; i++) {
		int sc = send(*unix_domain_connection_fds[i], data, strlen(data), 0);
		log_info("%d bytes send to client %d", sc, *unix_domain_connection_fds[i]);
	}
}

/**
* This function will be reading messages from the unix domain 
* socket client (Each spawned process will be having a connection
* to the unix domain server.)
* 
* @param void *
*/
void process_unix_domain_client(void *args) {
    int *sockfd = (int *) args;
    int rc, bytes_rec;
    log_info("waiting to read...");
    char buf[1024];    

    for (;;) {
        memset(buf, 0, 1024); 
        bytes_rec = recv(*sockfd, buf, sizeof(buf), 0);
        if (bytes_rec > 0){
            log_info("Recieved command from the client = %s", buf);
	        //memset(buf, 0, bytes_rec -1);
	        buf[bytes_rec] = '\0';
	        if (bytes_rec > 0) {
		        send_chat_message_to_all_unix_domain_client(buf);
		        log_info("Broadcasting done");  
	    	}
        } else {
	        log_info("Read nothing: %d bytes", bytes_rec);
	        break;
        }   
    }

}

/*
* This function handle the interrupt signal. In this function
* it will kill all the child processes and free up all the resources
* used by the application
*/
void sig_int_handler(int sig) {
	int     i;
    void    pr_cpu_time(void);

    /* terminate all children */
	for (i = 0; i < n_worker_process; i++)
	        kill(worker_pids[i], SIGTERM);
	while (wait(NULL) > 0) ;    /* wait for all children */
         
	if (errno != ECHILD)
        printf("wait error");

    free_resource_lock();
    log_info("Semaphore relased");
    threadpool_destroy(unix_domain_connection_pool ,1);
    log_info("Threadpool relased unix_domain_connection_pool");
	print_cpu_time();
	exit(0);
}

/*
* This function handles the TERM signal, which is being used
* by the spawned processes.
*/
void sig_term_handler(int sig) {
	log_info("Recieved TERM signal");
	destroy_list(connection_store);
    log_info("connection_store destroyed");
    threadpool_destroy(pool ,1);
    log_info("Threadpool destroyed");
	fflush(stdout);
	exit(0);
}

/**
 * This function will spwan new processes start accepting the client
 * connection in newly created child processes.
 *
 * @param serialNo
 * @param listenfd
 * @param addrlen
 * @return pid_t childprocess id (for parent process only) otherwise,
 * 	it will start accepting connection in the child processes.
 */
pid_t fork_worker_process(int serialNo, int listenfd, int addrlen) {
	pid_t   pid;
    if ((pid = fork()) > 0) {
            return (pid);            /* parent */
    }
    close(unix_domain_server_fd); // Child process no longer needed this.
    //unix_domain_client_fd = (int *)malloc(sizeof(int));
    unix_domain_client_fd = start_unix_domain_stream_client();
    log_info("Unix domain connected to server: %d for the process %d ", unix_domain_client_fd, getpid());
    worker_process_main(serialNo, listenfd, addrlen);     /* never returns */	
}





/**
 * This function will accept the connection
*
* @param serialNo
 * @param listenfd
 * @param addrlen
 */
void worker_process_main(int serial_no, int listenfd, int addrlen) {
	socklen_t clilen;	
	
	struct sockaddr *cliaddr;
	struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = &sig_term_handler;
	sigaction(SIGTERM, &action, NULL);
	cliaddr = malloc(addrlen);
	if ((pool = threadpool_create(num_thread_per_process, num_thread_per_process, 0)) == NULL) {
		log_error("Error creating threadpool");
		exit(0);
	}
    connection_store = createNewList();

    struct serv_commd_arg srvArg;
	srvArg.unixDomainFd = &unix_domain_client_fd;
	srvArg.connectionStore = connection_store;


    pthread_create(&unix_domain_server_command_process_thread, NULL, &process_server_command, (void *)&srvArg); 
	log_info("threadpool created for the process %d", getpid());
	log_info("Worker Process(%ld) Start accepting connections on TCP port: %d", (long) getpid(), tcp_port);
	for ( ; ; ) {
		clilen = addrlen;
		resource_lock_wait();
		int *connfd = (int *)malloc(sizeof(int));
		*connfd = accept(listenfd, cliaddr, &clilen);
		resource_lock_release();
		log_info("Got new connection sockfd : %d", *connfd);

		struct arg_struct arg;
		arg.inet_connfd = connfd;
		arg.unix_connfd = unix_domain_client_fd;
        threadpool_add(pool, &process_client, &arg, 0);
	}
}




/**
 * This function will handle the client request and respond with
 * suitable answers.
 * 
 * @param sockfd
 */
void process_client(void * arg) {

	int firstTime = 0;
	struct arg_struct *args = (struct arg_struct *)arg;
	int *sockfd = (int *) args->inet_connfd;
	int     ntowrite;
	ssize_t nread;
	char    line[MAXLINE];
	char currentNickName[25];
	log_info("Inside process client  args->inet_connfd %u, args->unix_connfd : %d", args->inet_connfd, args->unix_connfd);
	for ( ; ; ) {
		if (!firstTime) {
			send(*sockfd, CONNECT_MESSAGE, strlen(CONNECT_MESSAGE), 0);
			firstTime++;
		}
		if ((nread = recv(*sockfd, line, MAXLINE, 0)) < 0) {
			log_info("Connection closed by the client");
			if (nread == 0) {
				break;
			}           
		}
		if (nread > 0) {
			line[nread] = '\0';
			int cmdStatus = process_command(line, connection_store, args->unix_connfd, sockfd, currentNickName);
			if (cmdStatus == CLOSE_CONNECTION) {
				log_info("Connection disconnected by the client");
				break;
			} else if (cmdStatus == INVALID_CMD) {
				send(*sockfd, INVALID_COMMAND_MESSAGE, sizeof(INVALID_COMMAND_MESSAGE), 0);
			} else if (cmdStatus == ALREADY_JOINED_ERROR) {
				send(*sockfd, ALREADY_JOINED_ERR_MSG, sizeof(ALREADY_JOINED_ERR_MSG), 0);
			} else if (cmdStatus == LEAVE_ERROR) {
				send(*sockfd, NOT_YET_JOINED, sizeof(NOT_YET_JOINED), 0);
			} else if(cmdStatus == EXIT_ERROR) {
				send(*sockfd, EXIT_ERROR_MSG, sizeof(EXIT_ERROR_MSG), 0);
			} else if (cmdStatus == JOINED) {
				send(*sockfd, JOINED_MSG, sizeof(JOINED_MSG), 0);
			} else if(cmdStatus == PROCESSED) {
				send(*sockfd, PROMPT, sizeof(PROMPT), 0);
			}

			memset(line, '\0', sizeof(line));
    		memset(line, 0, MAXLINE);
		}
	}
	close(*sockfd);
	memset(currentNickName, '\0', sizeof(currentNickName));
	free(sockfd);
}





/**
* This function will start listening to the specified address and 
 returns the listening file descriptor.
*
* @param socklen_ * addrlen
* @return listenfd
*/
int listen_tcp(socklen_t * addrlen) {
	int fd = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in servaddr;
	bzero (&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(tcp_port);
	bind(fd, (struct sockaddr_in *) &servaddr, sizeof(servaddr));
	listen(fd, MAX_BACKLOG);
	log_info("Server start listening");
	return fd;
}
