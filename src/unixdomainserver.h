#ifndef _UNIX_DOMAIN_SERVER_H
#define _UNIX_DOMAIN_SERVER_H

#define SERVER_PATH "tpf_unix_sock.server"

int start_unix_domain_server();

#endif