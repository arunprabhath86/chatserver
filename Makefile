
CC=gcc
LIBS=-lpthread -lstdc++
CFLAGS=-w
SOURCES=$(wildcard src/*.c)
OBJECTS=$(patsubst %.c, %.o, $(SOURCES))
EXECUTABLE=bin/ChatServer

all: build $(EXECUTABLE)

$(EXECUTABLE):  $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) $(LIBS) -o $@

$(OBJECTS): src/%.o : src/%.c
	$(CC) $(CFLAGS) -c $< $(LIBS) -o $@

build:
	mkdir -p bin

clean:
	rm -rf $(EXECUTABLE) $(OBJECTS) bin tpf_unix_sock.client* tpf_unix_sock.server
