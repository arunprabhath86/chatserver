**ChatServer application**

Server creates a process pool of N processes.  Each process can create a pool of T threads. Each thread accepts a connection on a shared listening descriptor. 
Thread processes client's request as per the chat protocol described here: 
Client can send three messages :

1. JOIN name
2. LEAV 
3. CHAT targetname msg


In JOIN  message,  client sends  its  nickname  and  in CHAT  message  it  sends  the  nickname  of  the  person  to  whom  it needs to be delivered and the contents. 
When server receives a CHAT message, it sends the message on the socket matching the nickname.


**How to Compile**

1. Goto Chatserver folder
2. make clean 
3. make

On successfull compilation, bin folder will be generated, inside the bin folder, ChatServer executable will be generated.

**How to Run ChatServer**

The ChatServer executable accepts the following commandline arguments.

1. TCP port number to be listened
2. Number of processes to be created
3. Number of threads to be created to each spawned processes.

In order to execute the application, please type the following

1. Goto bin folder
2. ChatServer tcpPortNumber NumProcesses NumThreads

Eg: ChatServer 8080 3 3


**How to connect to ChatServer**

You can connect your chat server using telnet

ie,  telnet hostname tcpPortNumb

Eg: 

telnet localhost 8080

Trying ::1...

telnet: connect to address ::1: Connection refused

Trying 127.0.0.1...

Connected to localhost.

Escape character is '^]'.

You are connected. Please type JOIN <nickname> to the Chat System

You # JOIN b

You have joined the chat room

You # CHAT a "hello"

You # a << hi

You # LEAVE

Connection closed by foreign host.

